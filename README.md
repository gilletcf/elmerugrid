# ElmerUgrid

## Description

Codes and notebooks examples for pre-/post- processing of [Elmer/Ice](Elmer/Ice: http://elmerice.elmerfem.org/) simulations using [xugrid](https://deltares.github.io/xugrid/user_guide.html).

Mostly restricted/tested for 2D linear triangles elements for the moment.

Elmer/Ice can read and save (using xios) ugrid files; see the documentations for the [UGridDataReader.md](https://github.com/ElmerCSC/elmerfem/blob/devel/elmerice/Solvers/Documentation/UGridDataReader.md) and [XIOSOutputSolver.md](https://github.com/ElmerCSC/elmerfem/blob/devel/elmerice/Solvers/Documentation/XIOSOutputSolver.md) and 

Initial motivation was to implement a conservative interpolation scheme between Elmer grids, following Alauzet and Mehrenberger (2010) 

- References:   
	 - Alauzet and Mehrenberger, P1-conservative solution interpolation on unstructured triangular meshes, *Int. J. Numer. Meth. Engng.*, 2010   


## Content
- [notebooks](notebooks): notebooks to tests and illustrate the use of xugrid for pre- and post- processing
- [ElmerUgrid](ElmerUgrid): python codes developped for the interface with Elmer files and the conservative interpolation.
- [DATA](DATA): a directory used to store datas for the notebooks



