# notebooks

notebooks used for testing/illustration

## Content

- [ConservativeInterpolation](ConservativeInterpolation.ipynb): test the P1 conservative interpolation scheme
- [BedMachineToElmer](BedMachineToElmer.ipynb): test to import data from a structured grid to an ElmerGrid
- [ThicknessInterpolation](ThicknessInterpolation.ipynb): same as BedMachineToElmer.ipynb but with a field not defined everywhere, i.e. has data equal to the _FillValue attribute 
- [Elmer2Elmer](Elmer2Elmer.ipynb): test the conservative interpolation between Elmer grids
- [Abukill](Abukill.ipynb): same as Elmer2Elmer.ipynb but for simulations where part of the grid was passive
