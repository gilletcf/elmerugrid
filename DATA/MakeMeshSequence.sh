#!/bin/bash
#
# Create a sequence of mesh 
#
set -e

MeshSequence () {
	case=$1
   gmsh -2 square"$case"1.geo 
   ElmerGrid 14 2 square"$case"1.msh -autoclean
   ElmerGrid 2 5 square"$case"1

   for i in {2..5}
   do
	ip=$(($i-1))
	echo $ip
	gmsh -refine square"$case""$ip".msh -o square"$case""$i".msh
	ElmerGrid 14 2 square"$case""$i".msh -autoclean
	ElmerGrid 2 5 square"$case""$i"
   done
}


MeshSequence "A"
MeshSequence "B"
