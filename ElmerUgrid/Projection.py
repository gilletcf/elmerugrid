######################################################
# a collection of functions to project variables
# from faces to nodes and vice-versa on the same mesh
######################################################
import numpy as np
import numba as nb
from numba_celltree.constants import (FloatArray,IntArray,FloatDType,IntDType)
from .fem import detJ


@nb.njit(parallel=True)
def node_2_face_projection(
    node_values: FloatArray,
    faces: IntArray) -> FloatArray:
    ## the arithmetric mean; wihich is conservative for P1 interpolation

    n=np.shape(faces)[0]
    output=np.zeros(n,dtype=FloatDType)
    
    for e in nb.prange(n):
        nodesn=faces[e]
        nn=nodesn.size
        output[e]=node_values[nodesn].mean()
      
    return output

#TODO: to see how we could speed-up this with numba??
## for the moment the loop can not be parallelised..
def face_2_node_projection(
    face_values: FloatArray,
    grad_values: FloatArray,
    nn: IntDType,
    faces: IntArray,
    facesx: FloatArray,
    nodes: FloatArray) -> FloatArray:
    
    output=np.zeros(nn,dtype=FloatDType)
    weight=np.zeros(nn,dtype=FloatDType)

    ## 
    n=np.shape(faces)[0]
    for e in range(n):
        nodesn=faces[e]
        nodesx=nodes[e]
        detj=detJ(nodesx)
        grad=grad_values[e]
        face_val=face_values[e]
        if not np.isnan(face_val):
            for j in range(nodesn.size):
                dx=nodesx[j,:]-facesx[e,:]
                val=face_values[e]+ grad @ dx
                output[nodesn[j]] += np.abs(detj)*val
                weight[nodesn[j]] += np.abs(detj)

    result = np.where(weight > 0.0,output / weight,np.nan)

    return result

def face_2_node_projection_bounded(
    face_values: FloatArray,
    grad_values: FloatArray,
    MinMax: FloatArray,
    nn: IntDType,
    faces: IntArray,
    facesx: FloatArray,
    nodes: FloatArray) -> FloatArray:
    
    output=np.zeros(nn,dtype=FloatDType)
    weight=np.zeros(nn,dtype=FloatDType)

    ##
    n=np.shape(faces)[0]
    for e in range(n):
        nodesn=faces[e]
        nodesx=nodes[e]
        detj=detJ(nodesx)
        grad=grad_values[e]
        uG=face_values[e]
        if not np.isnan(uG):
            uk=np.empty(3)
        
            MaxVal=MinMax[e,1]
            MinVal=MinMax[e,0]

            for j in range(nodesn.size):
                dx=nodesx[j,:]-facesx[e,:]
                uk[j]= uG + grad @ dx

            sort_indexes=np.argsort(uk)
        
            uM=np.empty(3)
            uM[sort_indexes[2]]=min(uk[sort_indexes[2]],MaxVal)
            uM[sort_indexes[1]]=min(uk[sort_indexes[1]]+0.5*max(0.0,uk[sort_indexes[2]]-MaxVal),MaxVal)
            uM[sort_indexes[0]]=3*uG-uM[sort_indexes[1]]-uM[sort_indexes[2]]

            ## second sort utilde
            ut=np.empty(3)
            ut[sort_indexes[0]]=max(uM[sort_indexes[0]],MinVal)
            ut[sort_indexes[1]]=max(uM[sort_indexes[1]]-0.5*max(0.0,MinVal-uM[sort_indexes[0]]),MinVal)
            ut[sort_indexes[2]]=3*uG-ut[sort_indexes[0]]-ut[sort_indexes[1]]
        
            for j in range(nodesn.size):
                val=ut[j]
                output[nodesn[j]] += np.abs(detj)*val
                weight[nodesn[j]] += np.abs(detj)

    result = np.where(weight > 0.0,output / weight,np.nan)

    return result