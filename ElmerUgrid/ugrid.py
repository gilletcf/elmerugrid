#################################################
# a collection of functions to interface Elmer 
# with xugrid
#################################################

import xugrid as xu
from xugrid.ugrid.connectivity import counterclockwise
import numpy as np
import os
from typing import Any


#### redefine a class method to create a ugrid from Elmer mesh.
def fromElmer(
    cls,
    mesh_dir,
    name: str = "mesh2d",
    projected: bool = True,
    crs: Any = None,
    ):

    fill_value = -1
    #load mesh.nodes
    node_file=os.path.join(mesh_dir, "mesh.nodes" )
    nodes=np.loadtxt(node_file,dtype=np.double,usecols = (2,3))

    #load mesh.elements; restricted to 303 elments
    # to see of to parse mesh.header to be more generic???
    elem_file=os.path.join(mesh_dir, "mesh.elements" )
    elems=np.loadtxt(elem_file,dtype=np.int32,usecols = (2,3,4,5))
    face_node_connectivity=elems[:,1:]-1
    #ugrid requires conterclockwise connectivity...
    face_node_connectivity=counterclockwise(face_node_connectivity,fill_value,nodes)
    
    return cls(
            nodes[:,0],
            nodes[:,1],
            fill_value=fill_value,
            face_node_connectivity=face_node_connectivity,
            name=name,
            projected=projected,
            crs=crs,
        )

xu.Ugrid2d.FromElmer=classmethod(fromElmer)

def to_netcdf_forpv(self, *args, **kwargs):
        ds=self.to_dataset()
        #
        s = set(list(ds.indexes))
        coords = [x for x in list(ds.coords) if x not in s]
        #
        var_names=coords+list(ds.data_vars)
        ds[var_names].to_netcdf(*args, encoding={self.names[0]:{'dtype':'int32'},self.names[0]+'_face_nodes':{'dtype':'int32'},self.names[0]+'_edge_nodes':{'dtype':'int32'}},**kwargs)

xu.UgridDatasetAccessor.to_netcdf_forpv=to_netcdf_forpv
