#################################################
# a collection of functions related to fem
# mostly restricted to 3-nodes triangles for now
#################################################
import numpy as np
import numba as nb
from numba.np.extensions import cross2d
from numba_celltree.constants import PARALLEL, FloatArray,IntArray,FloatDType

@nb.njit()
def detJ(nodes:FloatArray) -> FloatDType:
    '''
    Returns the determinant of the Jacobian for the transformation between a 3-nodes triangle and
    the refrence triangle 

            Parameters:
                    nodes (FloatArray (3,2)): x and y coodinates of the nodes

            Returns:
                    v3 (FloatDType): det. of the Jacobian of the transformation
    '''
    if nodes.shape[0] != 3:
        raise Exception("Sorry, restricted to linear triangle elements") 
    v1=nodes[1,:]-nodes[0,:]
    v2=nodes[2,:]-nodes[0,:]
    v3=cross2d(v1,v2)
    return v3


@nb.njit()
def dBasisdx(nodes:FloatArray) -> FloatArray:
    '''
    Returns the basis function derivatives for a linear triangle
    dphi/d(x,y)=B.dphi/d(ksi,eta)

            Parameters:
                    nodes (FloatArray (3,2)): x and y coodinates of the nodes

            Returns:
                    dbdx (FloatArray (3,2)): basis function derivatives
    '''
    
    if nodes.shape[0] != 3:
        raise Exception("Sorry, restricted to linear triangle elements")
        
    B=np.array([[nodes[2,1]-nodes[0,1],nodes[0,1]-nodes[1,1]],
               [nodes[0,0]-nodes[2,0],nodes[1,0]-nodes[0,0]]],
              dtype=FloatDType)
    detj=detJ(nodes)
    B=B/detj
    grad=np.array([[-1.0,-1.0],
                   [1.0,0.0],
                   [0.0,1.0]],
                 dtype=FloatDType)
    
    dbdx=np.zeros((3,2))
    for k in range(3):
        dbdx[k,:]=B @ grad[k,:]
        
    return dbdx


@nb.njit(inline="always")
def Integrate_intersection(T: FloatArray,Y: FloatArray) ->  FloatArray:
    '''
    Integrate the basis functions of a triangle Y over the triangle T included in Y
    
            Parameters:
                    T (FloatArray (3,2)): node coordinates of triangle T
                    Y (FloatArray (3,2)): node coordinates of triangle Y
                    T in included in Y

            Returns:
                    M (FloatArray (3)): int. over triangle of the 3 basis functions of Y
    
    '''
    
    M=np.zeros(3,dtype=FloatDType)
    
    detJY=detJ(Y)
    detJT=detJ(T)

    xbar=T.sum(0) # 3*barycentric coordinates of T
    # values of the basis functions of Y at the barycenter of T  
    M[1]=(1.0/(3.0*detJY))*((Y[2,1]-Y[0,1])*xbar[0]+(Y[0,0]-Y[2,0])*xbar[1]+3.0*(Y[2,0]*Y[0,1]-Y[0,0]*Y[2,1]))
    M[2]=(1.0/(3.0*detJY))*((Y[0,1]-Y[1,1])*xbar[0]+(Y[1,0]-Y[0,0])*xbar[1]+3.0*(Y[0,0]*Y[1,1]-Y[1,0]*Y[0,1]))
    M[0]=1.0-M[2]-M[1]

    # Integration 1IP rule
    M=M*np.abs(detJT)/2.0
    
    return M



@nb.njit(parallel=PARALLEL, cache=True)
def TriIntegrate(
    values: FloatArray,
    faces: IntArray,
    nodes: FloatArray
    ) -> FloatDType:
    '''
    Integrate a nodal variable over the mesh
    The mesh must consist of 3-nodes triangles
    
            Parameters:
                    values (FloatArray (n_nodes)): nodal values
                    faces (IntArray (n_elements,nodes_per_element)): face_node_connectivity
                    nodes (FloatArray (n_elements,nodes_per_element,2)): face_node_coordinates
                    
            Returns:
                    output (FloatDType): integral of values over the faces
    '''
    
    output=np.double(0.0)

    nf=np.shape(faces)[0]
    for e in nb.prange(nf):
        nodesn=faces[e]
        nodesx=nodes[e]
        detj=detJ(nodesx)
        nn=nodesn.size

        ## integration over triangle 1IP rule
        val = np.sum(values[nodesn])/nn
        output += 0.5 * val * np.abs(detj)

    return output

@nb.njit(parallel=PARALLEL, cache=True)
def ComputeGrad( faces: IntArray, vertices: FloatArray,values: FloatArray) -> FloatArray:
    '''
    Compute the spatial derivatives of a quatity defined at the nodes.
    Restricted to linear interpolation so that derivatives are constant on the elements
    
            Parameters:
                    faces (IntArray (n_elements,nodes_per_element)): face_node_connectivity
                    vertices (FloatArray (n_elements,nodes_per_element,2)): face_node_coordinates
                    values (FloatArray (n_nodes)): nodal values

            Returns:
                    data_grad (FloatArray (n_elements,2)): the spatial derivatives defined on the elements
    '''
    n=np.shape(faces)[0]
    data_grad=np.zeros((n,2))
    for e in nb.prange(n):
        nodesn=faces[e]
        nodesx=vertices[e]
        detj=detJ(nodesx)
        dbdx=dBasisdx(nodesx)
        nodal_data=values[nodesn]
        data_grad[e,:]=dbdx.T @ nodal_data
    return data_grad




