import numba as nb
import numpy as np
import xugrid as xu
import xarray as xr

from scipy.sparse import *

from typing import Sequence, Tuple
from numba_celltree.geometry_utils import (Point,Vector,as_point,copy_vertices,polygon_area)
from numba_celltree.constants import (PARALLEL, FloatArray,IntArray,FloatDType,IntDType)
from numba_celltree.utils import (allocate_clip_polygon,copy)
from numba_celltree.algorithms.sutherland_hodgman import (inside,intersection,push_point)
from numba_celltree.celltree import(cast_vertices, cast_faces)

from .fem import (Integrate_intersection,ComputeGrad)
from .Projection import (face_2_node_projection,face_2_node_projection_bounded)

@nb.njit(inline="always")
def polygon_polygon_clip_area2(polygon: Sequence, clipper: Sequence) -> Tuple[int,FloatArray]:
    ## This is a copy of  https://github.com/Deltares/numba_celltree/blob/main/numba_celltree/algorithms/sutherland_hodgman.py
    ## modified to output the intersection polygon instead of the intersection area only;
    n_output = len(polygon)
    n_clip = len(clipper)
    subject = allocate_clip_polygon()
    output = allocate_clip_polygon()

    # Copy polygon into output
    copy(polygon, output, n_output)

    # Grab last point
    r = as_point(clipper[n_clip - 1])
    for i in range(n_clip):
        s = as_point(clipper[i])

        U = Vector(s.x - r.x, s.y - r.y)
        if U.x == 0 and U.y == 0:
            continue
        N = Vector(-U.y, U.x)

        # Copy output into subject
        length = n_output
        copy(output, subject, length)
        # Reset
        n_output = 0
        # Grab last point
        a = as_point(subject[length - 1])
        a_inside = inside(a, r, U)
        for j in range(length):
            b = as_point(subject[j])

            V = Vector(b.x - a.x, b.y - a.y)
            if V.x == 0 and V.y == 0:
                continue

            b_inside = inside(b, r, U)
            if b_inside:
                if not a_inside:  # out, or on the edge
                    succes, point = intersection(a, V, r, N)
                    if succes:
                        n_output = push_point(output, n_output, point)
                n_output = push_point(output, n_output, b)
            elif a_inside:
                succes, point = intersection(a, V, r, N)
                if succes:
                    n_output = push_point(output, n_output, point)
                else:  # Floating point failure
                    # TODO: haven't come up with a test case yet to succesfully
                    # trigger this ...
                    b_inside = True  # flip it for consistency, will be set as a
                    n_output = push_point(output, n_output, b)  # push b instead

            # Advance to next polygon edge
            a = b
            a_inside = b_inside

        # Exit early in case not enough vertices are left.
        if n_output < 3:
            return 0.0,output[:n_output]

        # Advance to next clipping edge
        r = s

    #area = polygon_area(output[:n_output])
    return n_output,output[:n_output]


@nb.njit(inline="always")
def tri_from_points(a:Point,b:Point,c: Point) -> FloatArray:
    ## convert points to a triangle
    Tri=np.empty((3, 2), dtype=FloatDType)
    Tri[0][0] = a.x
    Tri[0][1] = a.y
    Tri[1][0] = b.x
    Tri[1][1] = b.y
    Tri[2][0] = c.x
    Tri[2][1] = c.y
    return Tri


## 
@nb.njit(parallel=PARALLEL, cache=True)
def node_2_face_interpolation(
    vertices_a: FloatArray,
    vertices_b: FloatArray,
    faces_a: IntArray,
    faces_b: IntArray,
    indices_a: IntArray,
    indices_b: IntArray,
) -> Tuple[FloatArray,FloatArray,IntArray,IntArray]:
    '''
    Interpolation weights for a conservative interpolation between nodal values in "source" mesh
    to face values in "target" mesh.
    The "source" mesh must contain only triangles with linear interpolation.

    The interpolation matrix can be defined as a scipy coo_matrix as:
        M = coo_matrix((weights,(target_face_index,source_node_index)), shape=(target_grid.n_face,source_grid.n_node) )

    The intersection area, i.e. the area of an element in the target mesh covered by the source mesh, can be recovered as: 
        intersect_area=M*node_ones where node_ones is a vector of ones of lenght source mesh number of faces

    The interpolation is then obtained as:
        target_face_values=M*source_values/normalisation_area where the normalisation area can be the intersection area (eq. to cdo frac_area) or the target_elements area (eq. to cdo dest_area).

            Parameters:
                vertices_a (FloatArray (n_nodes,2)): target mesh node coordinates
                vertices_b (FloatArray (n_nodes,2)): source mesh node coordinates
                faces_a (IntArray (n_elements)): target mesh face_node connectivity
                faces_b (IntArray (n_elements)): source mesh face_node connectivity
                indices_a (IntArray (n_intersection)): target mesh faces intersected with faces in source mesh
                indices_b (IntArray (n_intersection)): source mesh faces intersecting  faces in starget mesh
                
                ## indices_a and indices_b can be computed with the celltree._locate_faces mesthod from xugrid
                indices_a,indices_b=source_grid.celltree._locate_faces(target_vertices,target_faces)
                    

            Returns:
                area (IntArray (n_intersection)): area of intersection between intersecting faces
                weights(IntArray (3*n_intersection)): the interpolation weights; i.e. the contribution of the source nodes to the area of intersection
                source_node_index (IntArray (3*n_intersection)): source node indexes
                target_face_index (IntArray (3*n_intersection)): target face indexes
                
                    
    '''
    n_intersection = indices_a.size
    area = np.zeros(n_intersection, dtype=FloatDType)
    weights= np.zeros(3*n_intersection, dtype=FloatDType)
    source_node_index= np.empty(3*n_intersection, dtype=IntDType)
    target_face_index= np.empty(3*n_intersection, dtype=IntDType)
    for i in nb.prange(n_intersection):
        face_a = faces_a[indices_a[i]]
        face_b = faces_b[indices_b[i]]
        p1 = copy_vertices(vertices_a, face_a)
        p2 = copy_vertices(vertices_b, face_b)
        n_out,polygon = polygon_polygon_clip_area2(p1, p2)

        for j in range(3):
            target_face_index[3*i+j]=indices_a[i]
            
        source_node_index[3*i:3*i+3]=face_b[0:3]
        #split output polygon to triangles
        if n_out >= 3:
            a = as_point(polygon[0])
            for j in range(1,n_out-1):
                b = as_point(polygon[j])
                c = as_point(polygon[j+1])
                Tri=tri_from_points(a,b,c)

                ## Integrate basis function of p2 over the intersection Tri
                M=Integrate_intersection(Tri,p2)
                area[i] += M[0]+M[1]+M[2]
                #### alternative compute triangle area
                #area[i] += polygon_area(Tri)
                weights[3*i:3*i+3] += M[0:3]
                
    return area,weights,source_node_index,target_face_index

## Face2face Interpolation
@nb.njit(parallel=PARALLEL, cache=True)
def face_2_face_interpolation(
    vertices_a: FloatArray,
    vertices_b: FloatArray,
    faces_a: IntArray,
    faces_b: IntArray,
    indices_a: IntArray,
    indices_b: IntArray,
) -> Tuple[FloatArray,IntArray,IntArray]:
    '''
    Interpolation weights for a conservative interpolation between face values in "source" mesh
    to face values in "target" mesh.

    The interpolation matrix can be defined as a scipy coo_matrix as:
        M = coo_matrix((weights,(target_face_index,source_face_index)), shape=(target_grid.n_face,source_grid.n_face) )

    The intersection area, i.e. the area of an element in the target mesh covered by the source mesh, can be recovered as: 
        intersect_area=M*face_ones where face_ones is a vector of ones of lenght source mesh number of faces

    The interpolation is then obtained as:
        target_face_values=M*source_values/normalisation_area where the normalisation area can be the intersection area (eq. to cdo frac_area) or the target_elements area (eq. to cdo dest_area).

            Parameters:
                vertices_a (FloatArray (n_nodes,2)): target mesh node coordinates
                vertices_b (FloatArray (n_nodes,2)): source mesh node coordinates
                faces_a (IntArray (n_elements)): target mesh face_node connectivity
                faces_b (IntArray (n_elements)): source mesh face_node connectivity
                indices_a (IntArray (n_intersection)): target mesh faces intersected with faces in source mesh
                indices_b (IntArray (n_intersection)): source mesh faces intersecting  faces in starget mesh
                
                ## indices_a and indices_b can be computed with the celltree._locate_faces mesthod from xugrid
                indices_a,indices_b=source_grid.celltree._locate_faces(target_vertices,target_faces)
                    
            Returns:
                weights(IntArray (n_intersection)): the interpolation weights; i.e. the contribution of the source nodes to the area of intersection
                target_face_index (IntArray (n_intersection)): target face indexes (==indices_a)
                source_face_index (IntArray (n_intersection)): source face indexes (==indices_b)
                
                    
    '''
    n_intersection = indices_a.size
    weights = np.empty(n_intersection, dtype=FloatDType)
    for i in nb.prange(n_intersection):
        face_a = faces_a[indices_a[i]]
        face_b = faces_b[indices_b[i]]
        a = copy_vertices(vertices_a, face_a)
        b = copy_vertices(vertices_b, face_b)
        polygon=allocate_clip_polygon()
        n_out,polygon = polygon_polygon_clip_area2(a, b)
        if n_out >= 3:
            weights[i] = polygon_area(polygon)
        else:
            weights[i]=0.0
        
    return weights,indices_a,indices_b


def node_2_node_interpolation(source:xu.UgridDataArray,
                              target:xu.Ugrid2d,
                              Bounded:bool,
                              area_threshold=1.0e-3):
    '''
    node_2_node_interpolation(source,target,Bounded,area_threshold):
        
        P1-conservative interpolation on unstructured triangular meshes
        Adapted from Alauzet and Mehrenberger, Int. J. Numer. Meth. Engng., 2010
        The mesh intersection is adapetd from :
         - xugrid: https://github.com/Deltares/xugrid 
         - numba_celltree tree: https://github.com/Deltares/numba_celltree

        Parameters:
                source (xugrid.UgridDataArray): source data to interpolate
                target (xu.Ugrid2d): the target mesh
                Bounded (bool): if True apply the minmax
                area_threshold=1.0e-3: if the ratio of the intersection area with the target element area 
                    is less than the threshold the value in the target elment is set to nan
                    
        Returns:
                result (xu.UgridDataArray): the source interpolated on the target mesh


        TODO: 
            - For non-matching boundaries, i.e. a element in the target mesh is not intersected by the source mesh, for
            the moment the normalisation is done uning the intersection area; this could be done instead using the target elment area
            (see e.g. cdo 'fracarea' and 'destarea' normalisation)
                        
    '''

    if not isinstance(source, xu.UgridDataArray):
        raise TypeError(f"Expected UgridDataArray, received: {type(source).__name__}")

    source_grid=source.grid

    if source.values.size != source.grid.n_node:
        raise Exception("input field should have the same size as the number of nodes in the grid") 

    if not isinstance(target, xu.Ugrid2d):
        raise TypeError(f"Expected Ugrid2d, received: {type(target).__name__}")

    target_grid=target

    ## compute the gradient of source values
    # gradient is constant by element
    source_val=source.values
    source_grad=ComputeGrad(faces=source_grid.face_node_connectivity,
                      vertices=source_grid.face_node_coordinates,
                      values=source.values)

    ## Mesh intersection
    target_vertices=cast_vertices(target_grid.node_coordinates)
    target_faces=cast_faces(target_grid.face_node_connectivity,target_grid.fill_value)

    source_vertices=cast_vertices(source_grid.node_coordinates)
    source_faces=cast_faces(source_grid.face_node_connectivity, source_grid.fill_value)

    #overlapping faces 
    target_indices,source_indices=source_grid.celltree._locate_faces(target_vertices,target_faces)

    ## Conservative node 2 faceweight sfo the nodal values
    area,weights,source_node_index,target_face_index = node_2_face_interpolation(
            vertices_a=target_vertices,
            vertices_b=source_vertices,
            faces_a=target_faces,
            faces_b=source_faces,
            indices_a=target_indices,
            indices_b=source_indices,
        )

    M = coo_matrix((weights,(target_face_index,source_node_index)), shape=(target_grid.n_face,source_grid.n_node) )
    M=M.tocsr()

    # MinMax values in the nodes belonging to the source faces intersected by the target_faces
    if (Bounded) :
        MinMax=IntersectionMinMax(node_values=source_val,
                                  target_faces= target_faces,
                                  source_faces= source_faces,
                                  target_indices= target_indices,
                                  source_indices= source_indices,
                                  area= area)

    ## Conservative face to face interpolation weightsfor the 2D gradient
    weights_f,target_face_index_f,source_face_index_f=face_2_face_interpolation(
            vertices_a=target_vertices,
            vertices_b=source_vertices,
            faces_a=target_faces,
            faces_b=source_faces,
            indices_a=target_indices,
            indices_b=source_indices,
        )


    Mf = coo_matrix((weights_f,(target_face_index_f,source_face_index_f)), shape=(target_grid.n_face,source_grid.n_face) )
    Mf=Mf.tocsr()

    ## normalisation weights
    node_ones=np.ones(source_grid.n_node,dtype=np.float64)
    intersect_area=M*node_ones
    area_ratio=intersect_area/target_grid.area

    ## The interpolation
    target_val=M*source_val
    target_val=np.where(area_ratio > area_threshold,target_val/intersect_area,np.nan)
    #target_val /= intersect_area
    
    target_grad=Mf*source_grad
    #target_grad = target_grad/np.expand_dims(intersect_area,axis=1)
    target_grad[:,0] = np.where(area_ratio > area_threshold,target_grad[:,0]/intersect_area,np.nan)
    target_grad[:,1] = np.where(area_ratio > area_threshold,target_grad[:,1]/intersect_area,np.nan)

    ## face 2 node projection
    if (Bounded) :
        node_val=face_2_node_projection_bounded(face_values=target_val,
                               grad_values=target_grad,
                               MinMax=MinMax,
                               nn=target_grid.n_node,
                               faces=target_grid.face_node_connectivity,
                               facesx=target_grid.face_coordinates,
                               nodes=target_grid.face_node_coordinates
                               )
    else:
        node_val=face_2_node_projection(face_values=target_val,
                               grad_values=target_grad,
                               nn=target_grid.n_node,
                               faces=target_grid.face_node_connectivity,
                               facesx=target_grid.face_coordinates,
                               nodes=target_grid.face_node_coordinates
                               )

    ## finally create a UgridDataArray
    result = xu.UgridDataArray(xr.DataArray(
                    data=node_val,
                    name=source.name,
                    dims=[target_grid.node_dimension],
                    attrs=dict(mesh=target_grid.name,location="node")),
                    target_grid)
    
    return result


@nb.njit()
def IntersectionMinMax(
    node_values: FloatArray,
    target_faces: IntArray,
    source_faces: IntArray,
    target_indices: IntArray,
    source_indices: IntArray,
    area: FloatArray
    ) -> FloatArray:   

    n_face=target_faces.shape[0]
    MinMax=np.empty((n_face,2))
    MinMax[:,0]=node_values.max()
    MinMax[:,1]=node_values.min()

    n_intersection = target_indices.size

    for ii in range(n_intersection):
        if (area[ii] > np.double(0.0)):
            target_face = target_faces[target_indices[ii]]
            source_face = source_faces[source_indices[ii]]
            local_values=node_values[source_face]
            MinMax[target_indices[ii],0]=min(MinMax[target_indices[ii],0],local_values.min())
            MinMax[target_indices[ii],1]=max(MinMax[target_indices[ii],1],local_values.max())

    return MinMax


def barycentric_interpolator(points, source_grid):
    '''
    barycentric_interpolator(points, source_grid) -> M,out: 
    
        Compute barycentric weights to interpolate nodal variables in the source grid to points
        For triangles with linear interpolation the barycentic weights are the shape functions

        With u a variable of size source_grid.n_node:
            - result=M*u returns the interpolated values at the points location (e.g. target mesh nodes)

        By construction the results is zero for points that are not found in the source mesh; special treatment can be obtain e.g.:
            - results[out]=np.nan
    
        Parameters:
                points (FloatArray (n_points,2)): (x,y) coordinates of target points
                source_grid (xugrid.Ugrid2d): the source grid

        Returns:
                M (scipy csr sparse matrix):  the interpolation weights
                out (IntArray): indexes of points non found in the source mesh
                
                    
    '''

    if not isinstance(source_grid, xu.Ugrid2d):
        raise TypeError(f"Expected xu.Ugrid2d, received: {type(source_grid).__name__}")

    ## locate points in mesh
    npoints=points.shape[0]
    points_indices=np.arange(npoints)
    face_index,weights=source_grid.compute_barycentric_weights(points)

    ## detect points that are not inside the mesh
    bindices_zero = (face_index > 0)
    in_mesh=points_indices[bindices_zero]
    out=points_indices[~bindices_zero]

    ## create the sparse matrix
    target_index=np.array([points_indices[in_mesh]]*3).T
    node_index=source_grid.face_node_connectivity[face_index[in_mesh]]

    M = coo_matrix((weights[in_mesh].flatten(),(target_index.flatten(),node_index.flatten())), shape=(npoints,source_grid.n_node) )
    M=M.tocsr()

    nout=len(out)
    if (nout > 0):
        print("WARNING: %i points have not been  found in the mesh "%nout)

    return M,out
        
    